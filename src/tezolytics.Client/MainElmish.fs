module tezolytics.Client.MainElmish

open Elmish
open Bolero
open Bolero.Html

type Message = SetFirstName of string | SetLastName of string   // Message
type InputModel = { label: string; value: string }
type Model = { firstName: string; lastName: string }            // Model

let initModel = { firstName = ""; lastName = "" }               // Init

let update message (model:Model) =                              // Update
  match message with
  | SetFirstName n -> { model with firstName = n }              // Command (not needed)
  | SetLastName n -> { model with lastName = n }

type InputComponent() =
  inherit ElmishComponent<InputModel, string>()

  override this.View model dispatch =
    label [] [
      text model.label
      input [
        attr.value model.value
        on.change (fun e -> dispatch (unbox e.Value))
      ]
    ]

let view model dispatch =                                       // View
    div [] [
      ecomp<InputComponent,_,_> {
        label = "First name: "
        value = model.firstName
      } (fun n -> dispatch (SetFirstName n))
      ecomp<InputComponent,_,_> {
        label = "Last name: "
        value = model.lastName
      } (fun n -> dispatch (SetLastName n))
      text (sprintf "Hello, %s %s!" (model.firstName |> string) (model.lastName |> string))
    ]

let program =                                                   // Program
  Program.mkSimple (fun _ -> initModel) update view

type MyElmishApp() =
  inherit ProgramComponent<Model, Message>()

  override this.Program = program
