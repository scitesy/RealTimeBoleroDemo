namespace tezolytics.Client

open Microsoft.AspNetCore.Blazor.Hosting
open Microsoft.AspNetCore.Components.Builder
open Microsoft.Extensions.DependencyInjection
open Bolero.Remoting.Client
open Blazor.Extensions

type Startup() =

    member __.ConfigureServices(services: IServiceCollection) =
        services.AddRemoting() |> ignore
        services.AddTransient<HubConnectionBuilder>()

    member __.Configure(app: IComponentsApplicationBuilder) =
        app.AddComponent<MainCrypto.MyElmishCryptoApp>("#main")
        //app.AddComponent<MainElmish.MyElmishApp>("#main")

module Program =

    [<EntryPoint>]
    let Main args =
        BlazorWebAssemblyHost.CreateDefaultBuilder()
            .UseBlazorStartup<Startup>()
            .Build()
            .Run()
        0
