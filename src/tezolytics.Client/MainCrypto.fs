module tezolytics.Client.MainCrypto

open System
open System.Globalization
open Elmish
open Bolero
open Bolero.Html
open Bolero.Json
open Bolero.Remoting
open Bolero.Remoting.Client
open Microsoft.AspNetCore.Components
open Microsoft.AspNetCore.SignalR.Client
open Microsoft.Extensions.DependencyInjection
open Microsoft.JSInterop
open Blazor.Extensions
open SharpVG
open XPlot
open XPlot.Plotly
open XPlot.Plotly.Html
open XPlot.Plotly.Graph
open XPlot.Plotly.Layout
open XPlot.GoogleCharts
open XPlot.GoogleCharts.Data
open XPlot.GoogleCharts.Deedle

type Page =
    | [<EndPoint "/">] Signal

type InputModel =
  {
    label: string
    value: float
    id: string
    chartName: string
    chartNode: Bolero.Node
  }

type Model =                                                                // Model
  {
    page: Page
    btcPrice: float
    btcPrices: (string * float) list
  }

let initModel =                                                             // Init
  {
    page = Signal
    btcPrice = float 0
    btcPrices = []
  }

type Message =                                                              // Message
  | SetPage of Page
  | SetBtcPrice of float
  | SetBtcPrices of (string * float)

let update message (model:Model) =                                          // Update
  match message with
  | SetPage page ->
    { model with page = page }, Cmd.none
  | SetBtcPrice price ->
    { model with btcPrice = price }, Cmd.none
  | SetBtcPrices price ->
    if model.btcPrices.Length = 21 then
      let removeAt index input =
          input
          |> List.mapi (fun i el -> (i <> index, el))
          |> List.filter fst |> List.map snd
      { model with btcPrices = (removeAt 0 model.btcPrices) } |> ignore
    { model with btcPrices = model.btcPrices @ [price] }, Cmd.none         // Command

let router = Router.infer SetPage (fun model -> model.page)

// chart code
let chartWidth = 1400
let chartHeight = 500

let toUnixDateTime (timestamp:int64) =
  let localDateTimeOffset = DateTimeOffset.FromUnixTimeMilliseconds(timestamp).ToLocalTime()
  localDateTimeOffset

let getUnixDateTimeFromTimestamp (ts:string) =
  let intTS = int64 ts
  let timestampDateTimeOffset = toUnixDateTime(intTS)
  timestampDateTimeOffset.DateTime

let getLongTimeDataValues prs =
  prs
  |> List.map (fun (x, y) ->
                let dt = getUnixDateTimeFromTimestamp x
                (dt.ToLongTimeString(), y))

let getFirstTimeStamp (prs:(string * float) list) =
  if prs.Length > 0 then
    let oTS, _ = prs.Head
    let oIntTS = int64 oTS
    (oIntTS.ToString())
  else
    ""

let getTimeDiffFloat currentTimeStamp originalTimeStamp =
  let currentTSDateTime = getUnixDateTimeFromTimestamp currentTimeStamp
  if originalTimeStamp <> "" then
    let oTS = int64 originalTimeStamp
    let oTSDateTimeOffset = toUnixDateTime(oTS)
    let oTSDateTime = oTSDateTimeOffset.DateTime
    let elapsedTicks = currentTSDateTime.Ticks - oTSDateTime.Ticks
    let elapsedSpan = TimeSpan(elapsedTicks)
    let tSeconds = Math.Abs(elapsedSpan.TotalSeconds)
    tSeconds
  else
    float 0

let getPairPointFloats prs =
  let firstTimeStamp = getFirstTimeStamp prs
  prs
  |> List.map (fun (x, y) -> ((getTimeDiffFloat x firstTimeStamp), float y))

let getUnPairPointData prs =
  let xs = prs |> List.map (fun (x, _) ->
                            let dt = getUnixDateTimeFromTimestamp x
                            dt.ToLongTimeString())
  let ys = prs |> List.map (fun (_, y) -> float y)
  xs, ys

let getGLineChart (prs:(string * float) list) =
  if prs.IsEmpty = false && prs.Length > 1 then
    let lineChart =
      Chart.Line (getLongTimeDataValues prs)
      |> Chart.WithOptions(Options(curveType = "function"))
    lineChart.Width <- chartWidth
    lineChart.Height <- chartHeight
    lineChart.GetHtml()
  else
    ""

let getTimeSeriesPlot (prs:(string * float) list) =
  if prs.IsEmpty = false && prs.Length > 1 then
    let layout = Layout(title = "Xplot Plotly Time Series Plot with datetime objects")
    let tail = prs.Tail
    let xs, ys = getUnPairPointData tail
    let scatter =
      XPlot.Plotly.Graph.Scatter (
        x = xs,
        y = ys
      )
      |> Chart.Plot
      |> Chart.WithLayout layout

    scatter.Width <- chartWidth
    scatter.Height <- chartHeight
    scatter.GetHtml()
  else
    ""

let getSVGPlot (prs:(string * float) list) =
  if prs.IsEmpty = false && prs.Length > 15 then
    let height = Length.Em (float 20)
    let width = Length.Em (float 85)
    let area = Area.create width height
    let svgPlot =
      SharpVG.Plot.plot (getPairPointFloats(prs))
        |> Svg.ofPlot
        |> Svg.withSize area
        |> Svg.toHtml "SVG Graph Example"
    svgPlot
  else
    ""

// elmish chart component
type InputComponent() =
  inherit ElmishComponent<InputModel, float>()

  override this.View model dispatch =
    label [] [
      text model.label
      table [attr.``class`` "table is-fullwidth"] [
        thead [][
          tr [][
            th [][
              text model.chartName
            ]
          ]
        ]
        tbody [][
          div [attr.id model.id][
            model.chartNode
          ]
        ]
      ]
    ]

// elmish view
let view model dispatch =                                               // View
    div [] [
      ecomp<InputComponent,_,_> {
        label = "Google Smoothed Line Chart"
        value = model.btcPrice
        id = "xplot"
        chartName = "Bitcoin Price"
        chartNode = getGLineChart model.btcPrices |> Node.RawHtml
      } (fun n -> dispatch (SetBtcPrice n))
      ecomp<InputComponent,_,_> {
        label = "Plotly Line Chart"
        value = model.btcPrice
        id = "xplotScatter"
        chartName = "Bitcoin Price"
        chartNode = getTimeSeriesPlot model.btcPrices |> Node.RawHtml
      } (fun n -> dispatch (SetBtcPrice n))
      ecomp<InputComponent,_,_> {
        label = "SharpVG SVG Scatter Plot"
        value = model.btcPrice
        id = "xplotScatter"
        chartName = "Bitcoin Price"
        chartNode = getSVGPlot model.btcPrices |> Node.RawHtml
      } (fun n -> dispatch (SetBtcPrice n))
    ]

// elmish application
type MyElmishCryptoApp() =
  inherit ProgramComponent<Model, Message>()

  [<Inject>]
  member val JsRuntime = Unchecked.defaultof<IJSRuntime> with get, set

  override this.Program =

    let krakenConnection =
      HubConnectionBuilder(this.JsRuntime)
        .WithUrl("http://127.0.0.1:5000/krakenCryptoHub", fun opt ->
          opt.Transport <- HttpTransportType.WebSockets
          opt.SkipNegotiation <- true
          opt.LogLevel <- SignalRLogLevel.Debug
        )
        .Build()

    let loginResponseHandler (connection :HubConnection) (success :bool) (id :string) =
      let getMessage =
        async {
          match success with
          | false ->
              return "Not logged in"
          | true ->
              return "Responded logged in"
        }
      let task = getMessage |> Async.StartAsTask 
      let awaitT (t: System.Threading.Tasks.Task) = t.ContinueWith (fun t -> ())
      let result = Async.AwaitTask(task)
      awaitT task

    krakenConnection.On<bool, string>("LoginResponse", fun success id -> loginResponseHandler krakenConnection success id) |> ignore

    let krakenStateHandler (krakenState:float) =
      let getKrakenState =
        async {
          printfn "%s" (krakenState.ToString())
          this.JsRuntime.InvokeAsync<string>("eval", "var div = document.getElementById('xplotScatter'); var x = div.getElementsByTagName('script'); eval(x[1].text);") |> Async.AwaitTask |> ignore
          this.JsRuntime.InvokeAsync<string>("eval", "var div = document.getElementById('xplot'); var x = div.getElementsByTagName('script'); eval(x[1].text);") |> Async.AwaitTask |> ignore
        }
      let task = getKrakenState |> Async.StartAsTask
      let awaitT (t: System.Threading.Tasks.Task) = t.ContinueWith (fun t -> ())
      awaitT task

    let bootSockets () =
      try
        krakenConnection.StartAsync() |> Async.AwaitTask |> ignore
        printfn "Connection started"
      with
      | ex ->
        printfn "Connection error %s" (ex.ToString())

    let krakenBTCPrice (initial:Model) =
      printfn "----------- in the kraken function -----------------"
      let krakenSub (dispatch: Message -> unit) =
        krakenConnection.On<string>("KrakenState", fun krakenState ->
          let krakenExchangeData = krakenState.Split ','
          let btcPr = krakenExchangeData.[2]
          let BTCPriceData =
              let btcP = (float btcPr)
              let timeStamp = krakenExchangeData.[1]
              (timeStamp, btcP)
          let ts, btc = BTCPriceData
          if btc > float 0 then
            (dispatch (SetBtcPrice btc))
            (dispatch (SetBtcPrices (BTCPriceData)))
          else
            printfn "%s" "btc kraken ain't no thing."
          krakenStateHandler btc
          ) |> ignore
      Cmd.ofSub krakenSub

    do bootSockets ()

    Program.mkProgram (fun _ -> initModel, Cmd.ofMsg (SetPage Signal)) update view          // Program
    |> Program.withRouter router
    |> Program.withSubscription krakenBTCPrice                                              // Subscription
