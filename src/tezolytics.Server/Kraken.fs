module Kraken

open System
open System.Threading
open System.Threading.Tasks
open Microsoft.AspNetCore.SignalR
open CryptoApi.Exchanges
open CryptoApi.Exchanges.Kraken.Parameters
open CryptoApi.Exchanges.Kraken.Parameters.SocketParams
open Hub

let krakenSocket = Kraken.WebSocketClient()

let krakenService (hubContext: IHubContext<KrakenCryptoHub, IKrakenClientApi>) =
  SocketExchange (fun (token) -> async {
    try
      do! krakenSocket.Connect token
      let sub : subscription = { name = "ticker" }
      let paramsT : SocketTickerParams = { event = (Action.Subscribe |> ActionToString); subscription = sub; pair = ["BTC/USD"] }
      do! krakenSocket.SubscribeTo(paramsT)
      krakenSocket.SetDidReceiveKrakenTicker (fun (update) ->
        let xtzData : string = String.Concat (update.Symbol, ",", getUnixDateTime DateTime.UtcNow, ",", update.Ask.Head.Price.ToString())
        hubContext.Clients.All.KrakenState(xtzData) |> ignore
      )
    with
    | ex ->
      System.Console.WriteLine ex.Message
  })
