module Hub

open System
open System.Threading
open System.Threading.Tasks
open Microsoft.AspNetCore.SignalR

type IKrakenClientApi =
  abstract member LoginResponse:bool * string -> Task
  abstract member Message:string -> Task
  abstract member KrakenState:string -> Task

let SocketExchange (fn:CancellationTokenSource -> Async<unit>) =
    let token = new CancellationTokenSource()
    Async.Start (fn(token), token.Token)
    ()

let getUnixDateTime (dateTime:DateTime) =
  let dateTimeOffset = DateTimeOffset(dateTime)
  let unixDateTime = dateTimeOffset.ToUnixTimeMilliseconds()
  unixDateTime

type KrakenCryptoHub () =
  inherit Hub<IKrakenClientApi> ()

  member this.Login (name :string) =
    async {
      let connectionId = this.Context.ConnectionId
      let success = true
      let clientId = "scott"
      if success then
        this.Clients.Client(connectionId).LoginResponse(true, clientId) |> ignore
      else
        this.Clients.Client(connectionId).LoginResponse(false, "") |> ignore
    }
    |> Async.StartAsTask

  override this.OnDisconnectedAsync(e:Exception) : Task =
    System.Console.WriteLine "*******signalr is disconnected!!!!!!!!!"
    this.OnConnectedAsync()

  member this.Send (message: string) = 
    async {
      System.Console.WriteLine message
      this.Clients.All.Message(message) |> ignore
    }
    |> Async.StartAsTask
