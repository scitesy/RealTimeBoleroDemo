namespace tezolytics.Server

open System
open System.IO
open System.Net.Mime
open System.Threading
open System.Threading.Tasks
open System.Text

open Microsoft.AspNetCore
open Microsoft.AspNetCore.Authentication.Cookies
open Microsoft.AspNetCore.Builder
open Microsoft.AspNetCore.Cors.Infrastructure
open Microsoft.AspNetCore.Hosting
open Microsoft.Extensions.DependencyInjection
open Microsoft.Extensions.Hosting
open Microsoft.AspNetCore.Http
open Microsoft.Extensions.Logging
open Microsoft.AspNetCore.SignalR
open Bolero
open Bolero.Remoting
open Bolero.Remoting.Server
open Bolero.Templating.Server

open Newtonsoft.Json.Serialization
open tezolytics
open CryptoApi.Exchanges
open Giraffe
open Kraken
open Hub

type KrakenCryptoService (hubContext :IHubContext<Hub.KrakenCryptoHub, Hub.IKrakenClientApi>) =
  inherit BackgroundService ()

  do Kraken.krakenService hubContext

  member this.HubContext :IHubContext<Hub.KrakenCryptoHub, Hub.IKrakenClientApi> =
    hubContext

  override this.ExecuteAsync (stoppingToken :CancellationToken) =
    Task.CompletedTask

module web =
  let webApp =
    choose [
      GET >=>
        choose [
          route "/" >=> text "Public endpoints"
        ]
    ]

type Startup() =

  let configureCors (builder: CorsPolicyBuilder) =
    builder.AllowAnyMethod()
      .AllowAnyHeader()
      .AllowCredentials()
      .WithOrigins(
         "http://127.0.0.1:5000",
         "https://127.0.0.1:5000",
         "http://localhost:5000",
         "https://localhost:5000",
         "ws://127.0.0.1:5000",
         "wss://127.0.0.1:5000",
         "ws://localhost:5000",
         "wss://localhost:5000"
      )
      |> ignore

  let errorHandler (ex:Exception) (logger:ILogger) =
    logger.LogError(EventId(), ex, "An unhandled exception has occurred while executing the request.")
    clearResponse >=> setStatusCode 500 >=> text ex.Message

  // This method gets called by the runtime. Use this method to add services to the container.
  // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
  member this.ConfigureServices(services: IServiceCollection) =
    services
      .AddAuthorization()
      .AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
        .AddCookie()
        .Services
#if DEBUG
      .AddHotReload(templateDir = "../tezolytics.Client")
#endif
      .AddCors()
      .AddLogging(fun logging ->
        logging.AddFilter("Microsoft.AspNetCore.SignalR", LogLevel.Debug) |> ignore
        logging.AddFilter("Microsoft.AspNetCore.Http.Connections", LogLevel.Debug) |> ignore
        logging.AddConsole() |> ignore
        logging.AddDebug() |> ignore
        logging.SetMinimumLevel(LogLevel.Debug) |> ignore
      )
      .AddHostedService<KrakenCryptoService>()
      .AddConnections()
      .AddSignalR(fun opt ->
        opt.KeepAliveInterval <- Nullable(TimeSpan.FromSeconds(5.0))
      )
      .AddJsonProtocol()
    |> ignore

    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
  member this.Configure(app: IApplicationBuilder, env: IWebHostEnvironment) =
    app
      .UseAuthentication()
      .UseRemoting()
#if DEBUG
      .UseHotReload()
#endif
      .UseClientSideBlazorFiles<Client.Startup>()
      .UseRouting()
      .UseEndpoints(fun endpoints ->
        endpoints.MapHub<KrakenCryptoHub>("/krakenCryptoHub") |> ignore
        endpoints.MapFallbackToClientSideBlazor<Client.Startup>("index.html") |> ignore)
      .UseGiraffe(web.webApp)
    |> ignore

module Program =

  [<EntryPoint>]
  let main args =
    WebHost
      .CreateDefaultBuilder(args)
      .UseStartup<Startup>()
      .Build()
      .Run()
    0
